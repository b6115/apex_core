/**
 * Created by alexanderbelenov on 15.05.2022.
 */

trigger SetPrimaryTrigger on AccountContact__c (before insert, before update) {
    if (CheckRecursive.firstcall) { return; }
    CheckRecursive.firstcall = true;
    if (Trigger.isInsert) {
        if (Trigger.isBefore) {
            SetPrimaryAccountContactHandler.beforeInsert(Trigger.new);
        }
    }
    else if (Trigger.isUpdate) {
        if (Trigger.isBefore) {
            SetPrimaryAccountContactHandler.beforeUpdate(Trigger.new, Trigger.old);
        }
    }

}