/**
 * Created by alexanderbelenov on 16.05.2022.
 */

public with sharing class SetPrimaryAccountContactHandler {
    public static void beforeInsert(List<AccountContact__c> newList) {
        new PrimaryAccountContactService().setPrimaryNewAccountContact(newList);
    }

    public static void beforeUpdate(List<AccountContact__c> newList, List<AccountContact__c> oldList) {
      new PrimaryAccountContactService().togglePrimaryAccountContract(newList, oldList);
    }
}