/**
 * Created by alexanderbelenov on 16.05.2022.
 */

public with sharing class PrimaryAccountContactService {

    public void setPrimaryNewAccountContact(List<AccountContact__c> newList) {
        AccountContractContractService accountContractContractService = new AccountContractContractService();

        for (AccountContact__c accountContact : newList) {
            Boolean contains = accountContractContractService.containsAccountContractsByContractId(accountContact.Contact__c);
            if (!contains) {
                accountContact.Is_Primary__c = TRUE;
            }
        }
    }

    public void togglePrimaryAccountContract(List<AccountContact__c> newList, List<AccountContact__c> oldList) {
        List<AccountContact__c> accountContactsToUpdate = new List<AccountContact__c>();
        for (AccountContact__c newEl : newList) {
            AccountContact__c oldEl = getById(newEl.Id, oldList);
            if (oldEl.Is_Primary__c && !newEl.Is_Primary__c) {
                accountContactsToUpdate.addAll(this.setPrimaryLastCreatedAccountContract(newEl));
            } else if (!oldEl.Is_Primary__c && newEl.Is_Primary__c) {
                accountContactsToUpdate.addAll(this.unsetPrimaryLastCreatedAccountContract(newEl));
            }
        }
        //update accountContactsToUpdate; // TODO: maybe return it higher?
        Database.update(accountContactsToUpdate, true);
    }

    private List<AccountContact__c> unsetPrimaryLastCreatedAccountContract(AccountContact__c accountContact) {
        accountContact = accountContact.clone();
        // unset true for the last created AccountContact
        List<AccountContact__c> lastCreated = getLastCreated(accountContact);
        for (AccountContact__c ac : lastCreated) {
            ac.Is_Primary__c = false;
        }
        return lastCreated;
    }

    private List<AccountContact__c> setPrimaryLastCreatedAccountContract(AccountContact__c accountContact) {
        accountContact = accountContact.clone();
        // set true for the last created AccountContact
        List<AccountContact__c> lastCreated = getLastCreated(accountContact);
        for (AccountContact__c ac : lastCreated) {
            ac.Is_Primary__c = true;
        }
        return lastCreated;
    }

    private AccountContact__c getById(Id id, List<AccountContact__c> accountContactList) {
        AccountContact__c result;
        for (AccountContact__c accountContact : accountContactList) {
            if  (accountContact.Id.equals(id)) {
                result = accountContact.clone();
                break;
            }
        }
        return result;
    }

    private List<AccountContact__c> getLastCreated (AccountContact__c accountContact) {
        List<AccountContact__c> lastCreated = [
                SELECT Id, CreatedDate
                FROM AccountContact__c
                WHERE Contact__c = :accountContact.Contact__c
                AND Id != :accountContact.Id
                ORDER BY CreatedDate DESC
                LIMIT 1
        ];

        return lastCreated;
    }

}