/**
 * Created by alexanderbelenov on 14.05.2022.
 */

public with sharing class Student {
    private String name;
    private Integer age;
    private String university;

    public Student(String name, Integer age, String university) {
        this.name = name;
        this.age = age;
        this.university = university;
    }

    public String greeting() {
        return 'Hallo';
    }


}