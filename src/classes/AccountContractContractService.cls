/**
 * Created by alexanderbelenov on 16.05.2022.
 */

public with sharing class AccountContractContractService {
    public Boolean containsAccountContractsByContractId(Id contractId) {
        Integer accCountNum = [
                SELECT COUNT()
                FROM AccountContact__c
                WHERE Is_Primary__c = TRUE
                AND Contact__c = :contractId
                LIMIT 1
        ];
        return accCountNum > 0;
    }
}